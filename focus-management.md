# Focus Management

## 1. What is Deep Work

### Question 1
What is Deep Work?
### Answer

Deep work is focusing without distraction when working on a demanding task, it's like having a hard focus on the task for hours.


## 4. Summary of Deep Work Book


### Question 2
Paraphrase all the ideas in the above videos and this one **in detail**.

### Answer
1. In the 1st video, the author says, the optimal duration for deep work is 1 hour or sometimes even 90 min, so that we can get at least 40-45 min of deep focus on the task.


2. In the 2nd video, the author says, having the Deadline reduces the frequency of taking a break and Deadlines serve as a motivation to work, as one looks to finish the work before the deadline.


3. The 3rd video is about how deep work can help us to complete difficult tasks. The intense focus causes the myelin to develop in relevant areas of the brain which allows the brain to fire faster and cleaner. . The three deep work strategies are:

* Schedule distraction
* Develop a deep work regularly
* Evening Shutdown



### Question 3
How can you implement the principles in your day to day life?
### Answer
* Making deadline for the tasks.
* Schedule  everything.
* get good sleep.

## 5. Dangers of Social Media

### Question 4
Your key takeaways from the video

* Paying less attention to the social media.
* And  i will try to reduce the amount of time spent on the social media.
* If you want to reduce anxiety and stress better don't use it.
* It plays with our mental health.
