
# Grit and Growth Mindset
## 1. Grit
#### Question 1
Paraphrase (summarize) the video in a few lines. Use your own words.
#### Answer
The video talks about the importance of grit and a growth mindset by Angela Lee Duckworth. she said that Grit is inversely proportional to talent. so grittier people are more likely to become successful than a talented ones. The only way to develop grit is through a growth mindset.
#### Question 2
What are your key takeaways from the video to take action on?
#### Answer
* Develop a growth mindset by focusing on effort and learning.
* Never take a failure as a permanent decision.
* Grit helps us to achieve more incredible things in our future selves.

## 2. Introduction to Growth Mindset
#### Question 3
Paraphrase (summarize) the video in a few lines in your own words.
#### Answer
The video explains the concept of a growth mindset, which refers to the belief that one's abilities and intelligence can be developed through effort and learning. The video suggests that having a growth mindset can lead to more tremendous success and well-being because it encourages a willingness to learn, embrace challenges, and persevere through setbacks.
#### Question 4
What are your key takeaways from the video to take action on?
#### Answer
* Seeking out new challenges and learning opportunities can help you continue to grow and develop.
* Skills are built so you can learn and grow.
* The process of getting better regardless of outcomes
* Seeking support and guidance from others.

## 3. Understanding Internal Locus of Control

#### Question 5
What is the Internal Locus of Control? What is the key point in the video?
#### Answer
Locus of control is the degree to which you believe you have control of your life. The key point in the video is that having an internal locus of control is important for staying motivated. They are also more likely to learn from their mistakes and continue working towards their goals even when faced with challenges or setbacks.
  

## 3. How to build a Growth Mindset

#### Question 6
Paraphrase (summarize) the video in a few lines in your own words.

#### Answer

In the video, the speaker Brendo Discusses the concept of a growth mindset and how it can be developed. A growth mindset is a belief that abilities and intelligence can be developed and improved through effort and learning.

#### Question 7

What are your key takeaways from the video to take action on?
#### Answer
* Developing a long-term growth mindset.
* Focusing on the process rather than the outcome.
* Learn from mistakes don't get demotivated by them.
* Accept feedback from others and try to improve yourselves.

## 4. Mindset - A MountBlue Warrior Reference Manual

#### Question 8

What are one or more points that you want to take action on from the manual? (Maximum 3)

#### Answer
1. I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
2. I will stay with a problem till I complete it. I will not quit the problem.
3. I will not leave my code unfinished till I complete the following checklist:
   * Make it work.
   * Make it readable.
   * Make it modular.
   * Make it efficient.