# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1
What is the Feynman Technique? Paraphrase the video in your own words.
### Answer
It is a method for learning and understanding complex concepts and ideas. It is named after the physicist **Richard Feynman**, who was known for his ability to explain difficult concepts simply and understandably.

### Question 2
What are the different ways to implement this technique in your learning process?
### Answer
  1) choose a concept to learn.
  2) Explain the concept using simple language.
  3) Return to the source material if you got stuck.
  4) Pinpoint any complicated terms and organize, analyze and simplify them.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 3
Paraphrase the video **in detail** in your own words.

### Answer
In the video, Barbara Oakley said that everyone should have a curiosity to learn anything. The video is about how to learn the concepts. We know that brain is a complicated structure, and its operation can be simplified into two different modes they are :

1) **Focus mode:** You turn your attention to the concept.
2) **Diffuse mode:** You will be in a relaxed state.

If we spend our time in a relaxed state (Diffuse mode), We don't get to pay attention to the concept. To overcome this issue we should think of a solution for the problem with active focus but also take breaks in between so that our passive brain can think of new ways to solve the given issue. It will help to come from diffusion mode to focus mode.

**Promodoro technique**, Where a timer is set for 25 minutes to focus on work. When the time is done, Have some relaxed fun for a few minutes. Relaxation is an important part of the learning process. It will help us to focus more on our learning.



### Question 4
What are some of the steps that you can take to improve your learning process?

These are the steps that I can take to improve my learning process.
1) Work with a **relaxed mind**.
2) I will try to work in a **Focus mode**.
3) Relax for a few minutes to focus on work in an effective way.
4) I will try not to get distracted.

## 3. Learn Anything in 20 hours

### Question 5
Your key takeaways from the video? Paraphrase your understanding.

### Answer

* Decide exactly what skill we want to develop and break it into smaller pieces. 
* Find out what **fundamental knowledge** is required to learn that content.
* Don't allow distractions or barriers while learning.
* Overcome the frustration of you not being able to learn something new.
* Practice daily, Atleast 45 minutes a day for a month will cover 20 hours of learning.

### Question 6

What are some of the steps that you can while approaching a new topic?

### Answer
These are the steps that I can take while approaching a new topic:

* Gather necessary **resources** about the topic.
* Break it down into smaller parts and learn them one by one.
* Practice daily at least 45 minutes a day.
* Remove distractions during practice.
