# Energy Management

## 1. Manage Energy not Time

### Question 1:
What are the activities you do that make you relax - Calm quadrant?
### Answer
* lisitening music and OST's
* watching movies and web series
* meditating for few minutes

### Question 2:
When do you find getting into the Stress quadrant?
### Answer
* If the work is heavy
* when i made any mistakes

### Question 3:
How do you understand if you are in the Excitement quadrant?
### Answer
* when the plan goes as expected.
* when i find something which i am looking for.


## 4. Sleep is your superpower

### Question 4
Paraphrase the Sleep is your Superpower video in detail.
### Answer
In this video Matt Walker shares the wonderfully good things that happen when you get sleep and the alarmingly bad things that happen when you don't, for both your brain and body. 

### Question 5
What are some ideas that you can implement to sleep better?
### Answer
* Sleep at regular times.
* Meditating before sleep.
* Mobile not to use before sleep.
* Maintain good temperature of the room


## 5. Brain Changing Benefits of Exercise
Video - 13:02 minutes - [https://www.youtube.com/watch?v=BHY0FxzoKZE](https://www.youtube.com/watch?v=BHY0FxzoKZE)

### Question 6
Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
### Answer
In the video wendy Suzuki explains the brain changing benefits of exercise those are memory gets better by exercising, Exercising reduces the risk of cognitive diseases.Exercising can increase focus span.


### Question 7
What are some steps you can take to exercise more?
### Answer
Give some exercise to the brain cells by meditating and jogging.