# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### Question 1
Your takeaways from the video (Minimum 5 points)
### Answer

* You have to start with a small action and that can leads to a large change over time.
* It's important to celebrate and acknowledge small successes
*  simple actions can lead to significant changes over time.
* Adding new habits to existing ones, so that they become routine.
* Small actions and tiny habits that can be taken consistently to build larger habits over time.

## 2. Tiny Habits by BJ Fogg - Core Message

### Question 2
Your takeaways from the video in as much detail as possible.
### Answer
* The video explains how BJ gave the (B=MAP) behavior model, where he explained that behavior change is possible not only by motivation.
* When you have made a mistake, you can certainly try again. They're so small that you don't have to depend on your fairly modest willpower.
  
### Question 3
How can you use B = MAP to make making new habits easier?
### Answer
* Identify what is motivating you to make the habit change.
* Celebrate minor victories.
* Break the change down into small and manageable steps.
### Question 4
Why it is important to "Shine" or Celebrate after each successful completion of a habit?
### Answer
* Celebrating or "shining" after each successful completion of a habit is important 

## 3. 1% Better Every Day Video
Video - 24:36 minutes - [https://www.youtube.com/watch?v=mNeXuCYiE0U](https://www.youtube.com/watch?v=mNeXuCYiE0U)

### Question 5
Your takeaways from the video (Minimum 5 points)
* the idea of "minimum viable effort" which is the least amount of effort you need to put in to see progress.
* Give your goals a time and place in the world, and set a fixed time to execute said tasks.
* Make good habits easier to follow.
* Plan to optimize the start not the finish.
* Be consistent, and don't break the chain of small habits.




## 4. Book Summary of Atomic Habits

### Question 6
Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.
### Answer
* Don't care about the outcome, Processes are about what you do. Identity is about what you believe.
* Prove it to yourself with small wins by proving the type of person you want to be.
* The ultimate form of intrinsic motivation is when a habit becomes part of your identity.

### Question 7
Write about the book's perspective on how to make a good habit easier.
### Answer
the book suggests several strategies, such as:
* breaking down the habit into smaller
* manageable steps
* making the habit more 

### Question 8
Write about the book's perspective on making a bad habit more difficult.
To make a bad habit more difficult, the book suggests several strategies such as
* creating an obstacle to the habit
* designing the environment in a way that makes the habit less convenient
* replacing the bad habit with a new, more desirable habit.

## 5. Reflection:

### Question 9:
Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying.
### Answer
I would like to solve at least one problem per day in the leet code. and I will make this a habit after this training done
### Question 10:
Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
### Answer

Feeling stressed, i want to reduce my stress level, so I want to start meditating at least 5-10 mins per day in the morning.