# Listening and Active Communication

## 1. Active Listening

#### Question 1
What are the steps/strategies to do Active Listening? (Minimum 6 points)
### Answer 
* Avoid getting distracted by our own thoughts, Instead focus on listening to what others are saying.
* Do not interrupt the other person while they are speaking. wait for them to finish then respond.
* Use door openers. Show the person that you're interested in listening to that topic and keep the other person talking.
* Show that you are listening with body language.
* Take notes during important conversations.
* Paraphrase what others have said to make sure you are on the same page.
  
## 2. Reflective Listening

#### Question 2
According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

### Answer 
* Avoidig the distraction and focusing on the conversation.
* The listner should try to mirror the mood of the speaker.
* Summarizing the content of the speaker.
* Responding to the speaker's specific point, without digressing to other subjects.
* We must quiet our minds and focus completely on the speaker's mood, reflecting his emotional state through words and non-verbal communication.
* We can summarise what the speaker said, using his own words.
* We have to respond to the speaker's specific point without deviating from other subjects.


## 3. Reflection

#### Question 3
What are the obstacles in your listening process?

### Answer 
* Get distracted by my own thoughts.
* Hurry and nervous
* Don't tend to take notes.

#### Question 4
What can you do to improve your listening?

### Answer

* Focusing one at a time.
* Practicing yoga and meditation.
* Try to stick to the schedule.

## 4. Types of Communication

#### Question 5
When do you switch to Passive communication style in your day to day life?

#### Question 6
When do you switch into Aggressive communication styles in your day to day life?

### Answer

* When got frustrated
* When lost the patience


#### Question 7

When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
### Answer
* When i am with friends Only

#### Question 8
How can you make your communication assertive? You can analyse the videos  and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

### Answer
* By clarifying what I know and what I need to know
* Use 'I' statements