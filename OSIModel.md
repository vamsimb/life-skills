# **OSI Model**

## Introduction

**OSI** stands for Open System interconnection mode developed by the international organization for standardization in 1984, Designed to show the flow of moving data from one software application of one computer to another software application of another computer.  It consists of seven layers and each layer has its own tasks, Which are performed independently.

## What is the OSI model

The Open Systems Interconnection (OSI) model describes seven layers computer systems use to communicate over a network. It was the first standard model for network communications, adopted by all major computer and telecommunication companies in the early 1980s. 

The modern Internet is not based on OSI, but on the simpler TCP/IP model. However, The OSI 7-layer model is still widely used, As it helps visualize and communicate how networks operate and helps isolate and troubleshoot networking problems.

## The Seven Layers of OSI

![Seven layers](https://tinypic.host/images/2022/11/25/WhatsApp-Image-2022-11-25-at-9.31.57-AM.jpg)

The seven abstraction layers of the OSI model can be defined as follows from top to bottom:

## 1. Application Layer

The application layer is the layer of the OSI model closest to the end user, which means both the OSI Application Layer and the user interact directly with a software application that implements a communication component between the client and server, Such as File Explorer and Microsoft Word. It provides protocols that enable these applications to send and receive information that is understandable to the end users.

![Application-Layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/koKt5UKczRq47xJsexfBV/c1e1b2ab237063354915d16072157bac/7-application-layer.svg)

* Application layer protocols include HTTP as well as SMTP (Simple Mail Transfer Protocol is one of the protocols that enables email communications).

## 2. Presentation Layer
This layer performs three significant functions:
 * Translation
 * Data compression
 * Encryption/Decryption

![Presentation-layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/60dPoRIz0Es5TjDDncEp2M/7ad742131addcbe5dc6baa16a93bf189/6-presentation-layer.svg)

It first receives data from the application layer and converts the user-end data into machine-understandable binary format. Then the data is compressed for faster transmission. This compression can be either lossy or lossless. Then to secure the data, it is encrypted. On the sender's side the data is encrypted. On the receiver's side the data is decrypted.


## 3. Session Layer
This layer is responsible for opening and closing communication between the two devices. The time between open and closed communication is known as the session. The session layer ensures that the session stays open long enough to transfer all the data being exchanged, and then promptly closes the session to avoid wasting resources.

![session layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/6jFRnaZSuIMoUzSotZXYbG/cc7a47d2b3f8d3e77b9ffbdb8b8d5280/5-session-layer.svg)

The session layer performs three functions:
* **Authentication**: It is essentially verifying who the user is. It is done by the usage of a username and password. Once the user is verified a connection is established with the server.

* **Authorization**: Authorization is checked, After authenticating the user. It is the process used by the server to determine whether the user has permission to access the file.

* **Session Management**: The session layer keeps a track of the data packets transmitted when downloading a file or using a browser. A web page contains information like text, images, etc. which are stored in a separate file on the web server. When a website is requested in the web server, It opens a separate session to the webserver to download each of these files. These files are received in the form of data packets. The session layer keeps track of these data packets.

## 4. Transport Layer
The Transport layer is responsible for end-to-end communication between the two devices. This includes taking data from the session layer and breaking it up into chunks called segments before sending it to layer 3. The transport layer on the receiving device is responsible for reassembling the segments into data the session layer can consume.
 
![Transport layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/1MGbIKcfXgTjXgW0KE93xK/64b5aa0b8ebfb14d5f5124867be92f94/4-transport-layer.svg) 

The transport layer controls the reliability of communications through **segmentation**, **flow control**, and **error control**.

* The data received from the session layer is divided into small data units called **segments**. Each segment contains a source and destination port number and a sequence number. Port number helps in directing each segment into the correct application while sequence number helps in reassembling these segments to form the correct message to the receiving user.

* The transport layer is also responsible for **flow control** and **error control**. Flow control determines an optimal transmission speed to ensure that a sender with a fast connection doesn’t overwhelm a receiver with a slow connection. The transport layer performs error control on the receiving end by ensuring that the data received is complete and requesting retransmission if it isn’t.

Protocols of the Transmission Layer are **Transmission Control Protocol (TCP)** and **User Datagram Protocol(UDP)**. TCP is used for connection-oriented transmission while UDP is for connectionless transmission. UDP is faster than TCP but does not give feedback on whether any data is missing or corrupt. TCP provides feedback so Lost data can be re-transmitted. 

## 5. Network Layer

The network layer is responsible for facilitating data transfer between two different networks. The network layer is unnecessary if the two devices communicating are on the same network. The network layer breaks up segments from the transport layer into smaller units, called **packets**, on the sender’s device, and reassembles these packets on the receiving device. The network layer also finds the best physical path for the data to reach its destination.

![Network layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/76JgEjycZl12c90UByKfJA/d6578bcd7b151c489e61f42227a45713/3-network-layer.svg)

Functions of the network layer are **Logical Addressing**, **Routing**, and **Path Determination**.

* IP addressing done in the network layer is called **logical addressing**. The network layer assigns the sender's and receiver's IP addresses in each segment to ensure each data packet reaches the correct destination.

* **Routing** is the method to move the packet from source to destination and it is based on the logical addressing format of IPv4 and IPv6. Based on IP addresses and masks, routing decisions are taken in the network layer.

* Any computer is connected to the internet server in a number of ways, Choosing the best possible path for data transmission from source to destination is called **path determination**.

## 6. Data Link Layer
The data link layer is very similar to the network layer, Except the data link layer facilitates data transfer between two devices on the SAME network. The data link layer takes packets from the network layer and breaks them into smaller pieces called **frames**. The data link layer is embedded as software in the Network Interface Card (NIC) of a computer. They are used to transfer data via local media such as copper wires, optical fibers, or air.

![Data layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/3MR4mPOwaos80t1annw7BG/8ea1c59ccfa1baf6e9738773daa30450/2-data-link-layer.svg)

The Data Link Layer allows the upper layers of the OSI Model to access media using techniques such as framing. It controls how data is placed and received from the media using techniques such as Media Access Control and Error Detection.

## 7. Physical Layer
This layer includes the physical equipment involved in the data transfer, such as the cables and switches.

The frames from the data link layer are in binary format (0s and 1s) called **bits**. The physical layer converts these bits into signals and transmits them over local media. The signals can be electrical signals, light signals, or radio signals.

![Physical layer](https://cf-assets.www.cloudflare.com/slt3lc6tev37/3m1ZkcaaBYHoodrEO3brv2/2819c4db294631b5753cd55de0c01bd9/1-physical-layer.svg)

At the receiver's end, The physical layer receives signals and converts them to bits, and passes it to the data link layer as a frame and then to higher layers in the form of packets and segments, And it reaches the application layer. The application layer protocols make the sender's message visible on the receiver side.

## Advantages of OSI Model

* It encrypts data for secure data transmission.
* Help network administrators in determining the required hardware and software to build their network.
* It supports both connection-oriented services and connectionless services.
* It is a generic model and acts as a guidance tool to develop any network model.
* Layers in the OSI model architectures can be distinguished and every layer has its own importance according to its interfaces, services, and protocols.
* The OSI divides all processes to make troubleshooting easier, As network administrators can troubleshoot issues more quickly and effectively by looking in a layer that is causing the issue rather than finding it in the entire network.

## References

* [OSI 7 layers | TechTerms](https://www.youtube.com/watch?v=vv4y_uOneC0)
* [What is the OSI model?](https://www.imperva.com/learn/application-security/osi-model/)
* [What are OSI layers?](https://www.cloudflare.com/learning/ddos/glossary/open-systems-interconnection-model-osi/)
* [Layers-of-osi-model | geeksforgeeks](https://www.geeksforgeeks.org/layers-of-osi-model/)
* http://www.tcpipguide.com/free/t_SessionLayerLayer5.htm
* [www.tutorialspoint.com](https://www.tutorialspoint.com/Advantages-and-Disadvantages-of-the-OSI-Model#:~:text=The%20advantages%20of%20the%20OSI%20model%20are&text=It%20distinctly%20separates%20services%2C%20interfaces,oriented%20services%20and%20connectionless%20services)