# Prevention of Sexual Harassment


## 1. What kinds of behavior cause sexual harassment?

### Answer

Sexual Harassment is any behavior that makes someone uncomfortable. It can occur in both public and private places. sexual Harassment takes many forms including:
 1. **Verbal**: It is a type of non-physical harassment that makes employees feel less comfortable, humiliated, and threatened. The most common forms of verbal harassment include:
   * Making sexual or gender-based jokes or remarks.
   * Commenting about a person’s clothing or body.
   * Using abusive language and offensive name-calling.
   * Teasing or asking sexually related questions.
2. **Visual**: It involves the use of visual media, such as photos, and videos to harass or intimidate someone. It can take many forms including:
   * Obscene posters
   * Drawing/Pictures
   * Sending sexual images or videos to another person.
3. **Physical**: Where someone inappropriately touches you against your will. The most common forms of physical harassment include:
   * Sexual assault.
   * Blocking a person's movements.
   * Inappropriate touching kissing or hugging.
   * Sexual gesturing, leering, or staring.

## 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

### Answer
It is important to act in this kind of situation. If I were in that situation.
* Inform the harasser: I will inform the person to stop this kind of behavior.
* Seek Help and Support: Take help from trusted persons like friends or supervisors.
* I will report the incidents to the relevant authorities.